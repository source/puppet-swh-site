mod 'gunicorn',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/puppet-swh-gunicorn',
    :branch => :control_branch,
    :default_branch => 'master'

mod 'mediawiki',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/puppet-swh-mediawiki',
    :branch => :control_branch,
    :default_branch => 'master'

mod 'postfix',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/puppet-swh-postfix',
    :branch => :control_branch,
    :default_branch => 'master'

mod 'uwsgi',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/puppet-swh-uwsgi',
    :branch => :control_branch,
    :default_branch => 'master'

mod 'apache',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppetlabs-apache',
    :tag => 'v12.2.0'

mod 'apt',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppetlabs-apt',
    :tag => 'v8.3.0'

mod 'archive',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppet-archive',
    :tag => 'v6.0.2'

mod 'augeas_core',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppetlabs-augeas_core.git',
    :tag => 'v1.5.0'

mod 'bind',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-inkblot-bind',
    :ref => 'anarcat'

mod 'cassandra',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppet-cassandra',
    :ref => 'master'

mod 'ceph',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-openstack-ceph',
    :ref => 'master'

mod 'concat',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppetlabs-concat',
    :tag => 'v7.1.1'

mod 'cron_core',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppetlabs-cron_core.git',
    :tag => 'v1.3.0'

mod 'cups',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-mosen-cups',
    :ref => 'fixup/new-stdlib-compat'

mod 'debconf',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-stm-debconf',
    :ref => 'v4.1.0'

mod 'docker',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppetlabs-docker',
    :ref => 'v4.1.2'

mod 'elasticsearch',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-elastic-elasticsearch',
    :ref => '6.4.0'

mod 'extlib',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppet-extlib',
    :tag => 'v5.3.0'

mod 'grafana',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppet-grafana',
    :ref => 'feature/puppet5-compat'

mod 'hitch',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-ssm-hitch',
    :ref => 'feature/additional-config-0.1.5'

mod 'icinga',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-icinga',
    :ref => 'v6.0.0'

mod 'icinga2',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-icinga-icinga2',
    :ref => 'swh/restore-puppet5'

mod 'icingaweb2',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-icinga-icingaweb2',
    :ref => 'v5.0.1'

mod 'inifile',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppetlabs-inifile',
    :ref => 'v5.2.0'

mod 'java',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppetlabs-java',
    :ref => 'backport/puppet5'

mod 'java_ks',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppetlabs-java_ks',
    :tag => 'v5.1.0'

mod 'kafka',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppet-kafka',
    :ref => 'v8.0.0'

mod 'keycloak',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-treydock-keycloak',
    # Need to upgrade to keycloak 12 before bumping to 7.x
    :ref => 'v6.26.0'

mod 'kmod',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-kmod.git',
    :ref => 'v3.2.0'

mod 'letsencrypt',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppet-letsencrypt',
    :ref => 'v7.0.0'

mod 'locales',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-saz-locales',
    :ref => 'v3.1.0'

mod 'mailalias_core',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppetlabs-mailalias_core.git',
    :tag => 'v1.2.0'

mod 'mount_core',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppetlabs-mount_core.git',
    :tag => 'v1.3.0'

mod 'mysql',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppetlabs-mysql',
    :ref => 'v12.0.1'

mod 'nginx',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppet-nginx',
    :ref => 'v3.3.0'

mod 'ntp',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppetlabs-ntp',
    :ref => 'v9.1.0'

mod 'php',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppet-php',
    :ref => 'v10.2.0'

mod 'postgresql',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppetlabs-postgresql',
    :ref => 'v10.5.0'

mod 'pgbouncer',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-covermymeds-pgbouncer',
    :ref => 'feature/plaintext-passwords'

mod 'puppet',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-theforeman-puppet',
    :ref => '20.0.0'

mod 'puppetdb',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppetlabs-puppetdb',
    :ref => 'v8.1.0'

mod 'memcached',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-saz-memcached',
    :ref => 'v7.0.0'

mod 'rabbitmq',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppet-rabbitmq',
    :ref => 'v11.1.0'

mod 'redis',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppet-redis',
    :ref => 'v8.2.0'

mod 'resolv_conf',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-saz-resolv_conf',
    :ref => 'v5.0.0'

mod 'ssh',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-saz-ssh',
    :ref => 'v13.0.0'

mod 'sshkeys_core',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppetlabs-sshkeys_core',
    :ref => 'v2.5.0'

mod 'stdlib',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppetlabs-stdlib',
    :ref => 'v9.7.0'

mod 'sudo',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-saz-sudo',
    :ref => 'v9.0.0'

mod 'sysctl',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-sysctl',
    :ref => '1.0.7'

mod 'systemd',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-camptocamp-systemd',
    # camptocamp/puppet-systemd 3.x dropped explicit systemctl daemon-reload
    # calls in favor of the built-in support in Puppet 6.1. We use Puppet 5.5.x,
    # so we can't upgrade this module.
    :ref => '2.12.0'

mod 'timezone',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-saz-timezone',
    :ref => 'v6.1.0'

mod 'unattended_upgrades',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppet-unattended_upgrades',
    :ref => 'v6.0.0'

mod 'varnish',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-claranet-varnish',
    :ref => 'bugfix/systemd-unit'

mod 'vcsrepo',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppetlabs-vcsrepo',
    :ref => 'v5.0.0'

mod 'zfs_core',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-puppetlabs-zfs_core',
    :ref => '1.0.0'  # Higher versions not compatible with puppet < 6.0

mod 'zookeeper',
    :git => 'https://gitlab.softwareheritage.org/swh/infra/puppet/3rdparty/puppet-deric-zookeeper',
    :ref => 'v1.2.1'
