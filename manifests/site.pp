node /^(pompidou|uffizi)\.(internal\.)?softwareheritage\.org$/
{
  include role::swh_hypervisor
}

node /^(hypervisor\d+|branly|mucem|chaillot)\.(internal\.)?softwareheritage\.org$/
{
  include role::swh_hypervisor_with_ceph
}

node 'pergamon.softwareheritage.org' {
  include role::swh_sysadmin
}

node 'grafana0.internal.admin.swh.network' {
  include role::swh_grafana
}

node 'tate.softwareheritage.org' {
  include role::swh_forge
}

node 'moma.softwareheritage.org' {
  include role::swh_reverse_proxy
}

node /^search-esnode\d\.internal\.softwareheritage\.org$/ {
  include role::swh_elasticsearch
}

node /^counters\d\.internal\.softwareheritage\.org$/ {
  include role::swh_counters_redis
}

node 'saatchi.internal.softwareheritage.org' {
  include role::swh_scheduler
}

node /^(belvedere|massmoca|albertina).(internal.)?softwareheritage.org$/ {
  include role::swh_database
  include profile::pgbouncer
}

node 'banco.softwareheritage.org' {
  include role::rancher_node_with_backup
}

node /^esnode\d+.(internal.)?softwareheritage.org$/ {
  include role::swh_elasticsearch
}

node /^kafka\d+\./ {
  include role::swh_kafka_broker
}

node /^cassandra\d?\d+\./ {
  include role::swh_cassandra_node
}

node /^runner\d+\./ {
  include role::gitlab_runner
}

node 'granet.internal.softwareheritage.org' {
  include role::swh_graph_backend
}

node 'maxxi.internal.softwareheritage.org' {
  include role::swh_graph_compression
}

node 'met.internal.softwareheritage.org' {
  include role::swh_provenance
}

node 'mam.internal.softwareheritage.org' {
  include role::nginx_only
}

node /^saam\.(internal\.)?softwareheritage\.org$/ {
  include role::rancher_node_with_multipath
}

node 'storage01.euwest.azure.internal.softwareheritage.org' {
  include role::swh_storage_cloud
}

node /^getty.(internal.)?softwareheritage.org$/ {
  include role::swh_journal_orchestrator
}

node /^ceph-osd\d+\.internal\.softwareheritage\.org$/ {
  include role::swh_ceph_osd
}

node /^ceph-mon\d+\.internal\.softwareheritage\.org$/ {
  include role::swh_ceph_mon
}

node /^ns\d+\.(.*\.azure\.)?internal\.softwareheritage\.org/ {
  include role::swh_nameserver_secondary
}

node 'thyssen.internal.softwareheritage.org' {
  include role::swh_ci_server
}

node 'riverside.internal.admin.swh.network' {
  include role::swh_sentry
}

node 'thanos.internal.admin.swh.network' {
  include role::swh_thanos
}

node 'thanos-compact.euwest.azure.internal.softwareheritage.org' {
  include role::swh_thanos_compact
}

node /^jenkins-debian\d+\.internal\.softwareheritage\.org$/ {
  include role::swh_ci_agent_debian
}

node /^jenkins-docker\d+\.internal\.softwareheritage\.org$/ {
  include role::swh_ci_agent_docker
}

node 'logstash0.internal.softwareheritage.org' {
  include role::swh_logstash_instance
}

node 'kibana0.internal.softwareheritage.org' {
  include role::swh_kibana_instance
}

node 'kelvingrove.internal.softwareheritage.org' {
  include role::swh_idp_primary
}

node 'giverny.softwareheritage.org' {
  include role::swh_desktop
}

node /^db\d\.internal\.staging\.swh\.network$/ {
  include role::rancher_node_with_database
}

node 'dali.internal.admin.swh.network' {
  include role::swh_admin_database
}

node "bardo.internal.admin.swh.network" {
  include role::swh_hedgedoc
}

node 'scheduler0.internal.staging.swh.network' {
  include role::swh_scheduler
  include profile::postgresql::client
}

node 'gateway.internal.staging.swh.network' {
  include role::swh_gateway
}

node /^storage\d\.internal\.staging\.swh\.network$/ {
  include role::rancher_node
}

node /^worker\d\.internal\.staging\.swh\.network$/ {
  include role::swh_worker_inria
}

node /^search-esnode\d\.internal\.staging\.swh\.network$/ {
  include role::swh_elasticsearch
}

node /^counters\d\.internal\.staging\.swh\.network$/ {
  include role::swh_counters_redis
}

node /^scrubber\d+\.internal\.(staging\.swh\.network|softwareheritage\.org)/ {
  include role::swh_scrubber_checkers
}

node /^rp\d\.internal\.(staging|admin)\.swh\.network$/ {
  include role::swh_reverse_proxy
}

node 'bojimans.internal.admin.swh.network' {
  include role::swh_netbox
}

node /^mirror-test\.internal\.staging\.swh\.network$/ {
  include profile::postgresql::client
}

node 'backup01.euwest.azure.internal.softwareheritage.org' {
  include role::zfs_snapshots_storage
}

node 'maven-exporter0.internal.staging.swh.network' {
  include role::swh_maven_index_exporter
}

node 'maven-exporter.internal.softwareheritage.org' {
  include role::swh_maven_index_exporter
}

node /^argo-worker\d+\.internal\.admin\.swh\.network$/ {
  include role::rancher_node
}

node /^rancher-node-.*\.internal\.((staging|admin)\.swh\.network|softwareheritage\.org)$/ {
  include role::rancher_node
}

node 'migration.internal.softwareheritage.org' {
  include role::swh_server  # Enable borgmatic backups
}

node default {
  include role::swh_base
}
